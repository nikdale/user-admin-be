import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';

import { LoginPage } from './containers/login';
import { PrivateRoute } from './components';
import { HomePage } from './containers/landing';
import './style/App.css';

function App() {
  return (
    <div className="App">
      <div className="container">
        <Router>
          <div>
            <PrivateRoute exact path="/" component={HomePage} />
            <Route path="/login" component={LoginPage} />
          </div>
        </Router>
      </div>
    </div>
  );
}

export default App;
