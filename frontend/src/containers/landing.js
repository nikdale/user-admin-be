import React from 'react';
import { Link } from 'react-router-dom';
import { UsersTable } from './usersTable';

class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {},
            users: []
        };
    }

    componentDidMount() {
        this.setState({
            user: JSON.parse(localStorage.getItem('user')),
            users: { loading: true }
        });
        // userService.getAll().then(users => this.setState({ users }));
    }

    render() {
        return (
            <div className="col-md-offset-3">
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse justify-content-md-center" id="navbarsExample08">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <Link className="nav-link" to="/login">Logout</Link>
                            </li>
                        </ul>
                    </div>
                </nav>

                <br></br>
                <UsersTable></UsersTable>
            </div>
        );
    }
}

export { HomePage };