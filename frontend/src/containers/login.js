import React from 'react';

import { userService } from '../services/user.services';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        // userService.logout();
        this.state = {
            // email: '',
            // password: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { id, value } = e.target;
        this.setState({ [id]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { emailInput, passwordInput } = this.state;
        if (!(emailInput && passwordInput)) {
            return;
        }

        this.setState({ loading: true });
        userService.login(emailInput, passwordInput)
            .then(
            user => {
                const { from } = this.props.location.state || { from: { pathname: "/" } };
                this.props.history.push(from);
            },
            error => {
                console.log(error);
                this.setState({ error, loading: false })
            }
        );
    }

    render() {
        const { password, email } = this.state;
        return (
            <div className="col-12 col-md-9 col-xl-4 py-md-3 pl-md-5 bd-content">
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label className="">Email address</label>
                        <input type="email" className="form-control" id="emailInput" aria-describedby="emailHelp" placeholder="Enter email" value={email}
                        onChange={this.handleChange} />
                        <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div className="form-group">
                        <label className="">Password</label>
                        <input type="password" className="form-control" id="passwordInput" placeholder="Password" value={password} 
                        onChange={this.handleChange} />
                    </div>
                    <div className="form-group form-check">
                        <input type="checkbox" className="form-check-input" id="rememberMe" />
                        <label className="form-check-label">Remember me</label>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>

        );
    }
}

export { LoginPage }; 