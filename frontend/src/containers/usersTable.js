import React from 'react';

import { userService } from '../services/user.services';
import Modal from '../components/modal';
import NewUserModal from '../components/new-user.modal';
import DeleteUserModal from '../components/delete-user.modal';
import '../style/table.css';

class UsersTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            show: false,
            showNewUserModal: false,
            userId: null,
        }

        this.showModal = e => {
            let id = null;
            let currentUserData = null;
            const userId = e.target.attributes.userid;
            const allUserData = this.state.data;

            if (userId) {
                id = userId.value
                for (const userData of allUserData) {
                    if (userData._id === id) {
                        currentUserData = userData;
                    }
                }
            };

            this.setState({
                show: !this.state.show,
                showNewUserModal: false,
                showDeleteUserModal: false,
                userId: id,
                currentUserData
            });
        };

        this.showNewUsrModal = e => {
            this.setState({
                showNewUserModal: !this.state.showNewUserModal,
                show: false,
            })
        };
        
        this.blockSwitch = e => {
            const userId = e.target.attributes.userid.value;
            userService.blockSwitch(userId);
        };
        
        this.showDeleteUsrModal = e => {
            let id = null;
            let currentUserData = null;
            if (e.target) {
                const userId = e.target.attributes.userid;
                const allUserData = this.state.data;
                if (userId) {
                    id = userId.value
                    for (const userData of allUserData) {
                        if (userData._id === id) {
                            currentUserData = userData;
                        }
                    }
                };
            }

            this.setState({
                showDeleteUserModal: !this.state.showDeleteUserModal,
                show: false,
                showNewUserModal: false,
                userId: id,
                currentUserData
            });
        };

        this.getAllData = async function() {
            const userData = await userService.getAll();
            userData.forEach(user => {
                if (user.blocked) {
                    user.blockedText = "Blocked"
                } else {
                    user.blockedText = "Not blocked"
                }
                return user;
            });
            this.setState({
                data: userData
            });
        }
    }

    async componentDidMount() {
        await this.getAllData();
    }

    async componentDidUpdate(prevProps, prevState, snapshot) {
        await this.getAllData();
    }

    render() {
        let userData = this.state.data.map((userData, i) => {
            let blockButton = "Block";
            let blockDataTarget = "#block";
            if (userData.blocked) {
                blockButton = "Unblock";
                blockDataTarget = "#unblock";
            }
            return (
                <tr key={i}>
                    <td>{userData.name}</td>
                    <td>{userData.surname}</td>
                    <td>{userData.email}</td>
                    <td>{userData.blockedText}</td>

                    <td><p data-placement="top" data-toggle="tooltip" title="Edit"><button userid={userData._id} onClick={e => { this.showModal(e); }} className="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span userid={userData._id} >Edit</span></button></p></td>
                    <td><p data-placement="top" data-toggle="tooltip" title={blockButton}><button userid={userData._id} onClick={e => { this.blockSwitch(e); }} className="btn btn-warning btn-xs" data-title={blockButton} data-toggle="modal" data-target={blockDataTarget} ><span userid={userData._id}>{blockButton}</span></button></p></td>
                    <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button userid={userData._id} onClick={e => { this.showDeleteUsrModal(e); }} className="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span userid={userData._id}>Delete</span></button></p></td>
                </tr>
            );
        });

        return (
            <React.Fragment>
                <Modal onClose={this.showModal} show={this.state.show} userData={this.state.currentUserData}>
                        Update user data
                </Modal>
                <NewUserModal onClose={this.showNewUsrModal} showNewUserModal={this.state.showNewUserModal}>
                        Create new user
                </NewUserModal>
                <DeleteUserModal onClose={this.showDeleteUsrModal} showDeleteUserModal={this.state.showDeleteUserModal} userData={this.state.currentUserData}>
                        Delete
                </DeleteUserModal>
                <div className="container">
                    <div className="row">
                        <div className="table-responsive">
                            <table id="mytable" className="table table-bordred table-striped">
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Blocked</th>

                                        <th>Edit</th>
                                        <th>Block</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {userData}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div>
                        <button className="btn btn-success btn-xs" onClick={e => { this.showNewUsrModal(e); }} ><span>Add new user</span></button>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export { UsersTable }; 