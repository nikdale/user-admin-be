import React from "react";
import '../style/modal.css';
import { userService } from '../services/user.services';

export default class DeleteUserModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { id, value } = e.target;
        this.setState({ [id]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const userId = this.props.userData._id;
        if (!userId) {
            return;
        }

        this.setState({ loading: true });
        userService.deleteUser(userId)
            .then(
                response => {
                    this.onClose(e);
                    alert(response.detail);
                },
                error => {
                    console.log(error);
                    this.onClose(e);
                    alert(error);
                    this.setState({ error, loading: false })
                }
            );
    }

    onClose = e => {
        this.props.onClose(e);
    };

    render() {
        if (!this.props.showDeleteUserModal) {
            return null;
        }

        const { name, surname, email} = this.props.userData;

        return (
            <div className="modal1" id="modal1" userid={this.props.userid}>
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">{this.props.children + " " + email}</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={e => { this.onClose(e); }}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body bg-danger">
                            <p className="text-warning">Delete:</p>
                            <hr></hr>
                            <p className="text-light">{name}</p>
                            <p className="text-light">{surname}</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-success" data-dismiss="modal" onClick={e => { this.onClose(e); }}>Close</button>
                            <button type="button" className="btn btn-danger" onClick={e => { this.handleSubmit(e); }}>DELETE</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
