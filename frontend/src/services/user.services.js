import { authHeader } from '../helpers/auth-headers';

export const userService = {
    login,
    logout,
    register,
    getAll,
    blockSwitch,
    deleteUser
};

async function login(email, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify({ email, password })
    };

    return await fetch(`http://127.0.0.1:3000/api/users/login`, requestOptions)
        .then(handleResponse)
        .then(user => {
            if (user) {
                user.authdata = window.btoa(email + ':' + password);
                localStorage.setItem('user', JSON.stringify(user));
            }
            return user;
        });
}

async function register(userData) {
    const requestOptions = {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify(userData)
    };

    return await fetch(`http://127.0.0.1:3000/api/users/register`, requestOptions)
        .then(handleResponse)
        .then(response => {
            return response;
    });
}

function logout() {
    localStorage.removeItem('user');
}

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
    };

    return fetch(`http://127.0.0.1:3000/api/users/getAll`, requestOptions)
        .then(handleResponse);
}

async function blockSwitch(userId) {
    if (!userId) {
        throw new Error('No userId');
    }
    const requestOptions = {
        method: 'PATCH',
        headers: authHeader(),
    };

    return await fetch(`http://127.0.0.1:3000/api/users/block?id=${userId}`, requestOptions)
        .then(handleResponse)
        .then(response => {
            return response;
    });
}

async function deleteUser(userId) {
    if (!userId) {
        throw new Error('No userId');
    }
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader(),
    };

    return await fetch(`http://127.0.0.1:3000/api/users/delete?id=${userId}`, requestOptions)
        .then(handleResponse)
        .then(response => {
            return response;
    });
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401 || response.status === 400) {
                // auto logout if 401 response returned from api
                // logout(); // let's dont do that, it's boring
                // location.reload(true);   
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}