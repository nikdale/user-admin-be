const jwt = require('jsonwebtoken');

const config = require('../config');

const secret = config.jwt.secret;

auth = (req, res, next) => {
  const authHeader = req.get('Authorization');
  req.isAdmin = false;
  if (!authHeader) {
    req.isAuth = false;
    return next();
  }
  const token = authHeader.split(' ')[1];
  if (!token || token === '') {
    req.isAuth = false;
    return next();
  }
  let decodedToken;
  try {
    decodedToken = jwt.verify(token, secret);
  } catch (err) {
    req.isAuth = false;
    return next();
  }
  if (!decodedToken) {
    req.isAuth = false;
    return next();
  }
  if (decodedToken._doc.admin) {
    req.isAdmin = true;
  }
  req.isAuth = true;
  req.userId = decodedToken.userId;
  next();
};

module.exports = { auth };
