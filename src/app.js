const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const cors = require('cors');

const usersRoute = require('./modules/users');
const User = require('./models/user');
const Roles = require('./common/enum');

const app = express();
const port = process.env.PORT || 3000;
app.use(cors());
app.use(bodyParser.json());
app.use(cookieParser());

app.use('/api/users', usersRoute);

mongoose.Promise = global.Promise;
mongoose.set('useCreateIndex', true);
mongoose
  .connect(
    // 'mongodb+srv://nikdale:nikdale@cluster0-styjb.mongodb.net/useradministration?retryWrites=true',
    `mongodb://localhost:27017/useradministration`,
    { useNewUrlParser: true }
  )
  .then(
    () => {
      console.log('Connected to MongoDB');
      app.listen(port, () => {
        console.log(`Server running on port ${port}`);
      });
      // Create first admin user!
      User.findOne({email: "admin@yopmail.com"}, async function(err, user) {
        if (err) {
          console.log(err);
        }
        if (!user) {
          const user = new User({
            name: "Admin",
            surname: "User",
            email: "admin@yopmail.com",
            password: "admin123",
            roles: [Roles.User, Roles.Admin]
          });
          const persistedUser = await user.save();
          console.log("First user created: ", persistedUser.name);
        }
      })
      // **
    },
    (err) => console.log('Error connecting to mongoDB', err)
  );

mongoose.set('useFindAndModify', false);

module.exports = { app };
