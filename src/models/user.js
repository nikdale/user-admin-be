

const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcryptjs');

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 2,

  },
  surname: {
    type: String,
    required: true,
    minlength: 2,
  },
  email: {
    type: String,
    required: true,
    minlength: 4,
    trim: true, // Removes whitespace
    unique: true, // We use this flag to activate mongoose-unique-validator
    sparse: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 8,
    // select: false
  },
  roles: {
    type: [String],
    required: true,
  },
  blocked: {
    type: Boolean,
    required: true,
    default: false
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
});

// pushes inque plugin to schema
UserSchema.plugin(uniqueValidator);

UserSchema.pre('save', function(next) {
  let user = this;
  if (!user.isModified('password')) {
    return next();
  }

  bcrypt
    .genSalt(12)
    .then((salt) => {
      return bcrypt.hash(user.password, salt);
    })
    .then((hash) => {
      user.password = hash;
      next();
    })
    .catch((err) => next(err));
});

UserSchema.pre('findOneAndUpdate', function(next) {
  const changes = this.getUpdate().$set;
  if (!changes) {
    return next();
  }
  const password = this.getUpdate().$set.password;
  let user = this.getUpdate().$set;

  bcrypt
    .genSalt(12)
    .then((salt) => {
      return bcrypt.hash(user.password, salt);
    })
    .then((hash) => {
      user.password = hash;
      next();
    })
    .catch((err) => next(err));
});

UserSchema.index({
  name: 'text',
  surname: 'text',
  email: 'text',
}, {name: 'search'})

module.exports = mongoose.model('User', UserSchema);

