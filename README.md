User Administration System Back End application

# Starting the app

1) Clone the app with `git clone` or download it
2) Go to root of the project `cd user-admin-be`
3) Run `npm install` to download (add) dependencies
4) Start the app with `npm start`, app will be running on localhost:3000

## Run the Client app

1) Open new terminal windows
2) Locate project root and run `cd frontend` to locate client app
3) Run `npm install` to download (add) dependencies of client app
4) Run `npm start`, app will be running on localhost:4000

### Admin User login:
```
admin@yopmail.com
admin123
```


### PostMan collection: https://www.getpostman.com/collections/d4d0f1b862779e41723b


Assignment:
```
Simple User Administration
    User shoud have: First Name, Last Name, Email address, password and role (user, admin).

    * User - CRUD + Block
    * Sorting by name, last name or email
    * Search
```

```
Evo, smislili smo neki test, u pitanju je jednostavna administracija korisnika. Treba da koristis node.js, mongodb i react (taman da se malo podsetis :).

Funkcionalnosti:
- Dodavanje, brisanje i zabrana korisnika
- Korisnik treba da ima: ime, prezime, email, password i nivo pristupa (admin i obican korisnik). Naravno treba uraditi validaciju forme za unos i editovanje.
- Sortiranje po imenu, prezimenu ili email-u
- Pretraga
```